import os
import shutil
import sys
import datetime
from PyQt5.QtGui import *
from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
from PyQt5.Qt import *
import sip
import numpy as np
import cv2
import pprint
import matplotlib.pyplot as plt
from matplotlib import patches
from mpl_toolkits.mplot3d import Axes3D
import json
import pandas as pd
import sklearn
from sklearn.decomposition import PCA
import math
from scipy.stats import norm
from scipy.optimize import curve_fit
import random
from scipy import stats

class PlotWindow:
    def __init__(self):
        fig = plt.figure()
        self.ax = fig.add_subplot(projection='3d')
#        self.ax = fig.add_subplot(111)
#        self.ax.set_aspect('equal')
        self.ax.set_xlabel("B")
        self.ax.set_ylabel("G")
        self.ax.set_zlabel("R")
#        self.ax.set_xlim(15, 40)
#        self.ax.set_ylim(0, 20)
        # self.ax.set_xlim(0, 160)
        # self.ax.set_ylim(0, 160)
        # self.ax.set_zlim(0, 160)

        self.sum_b = 0
        self.sum_g = 0
        self.sum_r = 0
        self.kk = 0

        self.path_img = "KEKv1.0Q2_vi"
        self.path_pos = "SMD_position_s.json"
        self.path_color = "SMD_color.json"
        self.read_img(self.path_img, self.path_pos)
        self.json(self.path_img, self.path_pos, self.path_color)

        
    def read_img(self, path_img, path_pos):
        f = open(path_pos, "r")
        df = json.load(f)
        f.close()
        h_ref, w_ref, self.L_SMD, self.S_SMD = df["size"]
        img = cv2.imread("./module_img/{}_trimmed.jpg".format(path_img))
        h,w,d = img.shape[:3]
        scale = h_ref/h
#        print("scale",scale)
        self.resize_img = cv2.resize(img, dsize=None, fx=scale, fy=scale)

    def json(self, path_img, path_pos, path_color):

        fr = open(path_pos, "r")
        jfr = json.load(fr)
        fw = open(path_color, "w")
        jfw = {path_img:{"0":[0,0,0]}}
        df = []
        df_ex = []
        df_SMD = []
        b = []
        g = []
        r = []
        b_PCB = []
        g_PCB = []
        r_PCB = []
        color = []
        color_ex = []
        color_SMD = []
        RGB = 256
        tot_b = 0
        tot_g = 0
        tot_r = 0
        diff_b = 0
        diff_g = 0
        diff_r = 0
        diff_bg = 0
        diff_gr = 0
        diff_rb = 0
        N = 0
#        self.def_SMD = [28, 64, 96, 134]
        self.def_SMD = []

# extract half of PCB points
        PCB_list = []
        for ll in range(60):
            PCB_list.append(ll+200)
        rand = random.sample(PCB_list, 30)
        rand.sort()
        not_rand = []
        n_rand = 0
        
        for key in jfr:
            if not key == "size":
                self.sum_b = 0            
                self.sum_g = 0            
                self.sum_r = 0            
                self.kk = 0            
                position = jfr[key]
                #    print(position)
                if jfr[key][2] == "s":
                    SMD_size = self.S_SMD
                else:
                    SMD_size = self.L_SMD
                ran = 1+2*SMD_size
                for i in range(ran):
                    for j in range(ran):
                        px = self.resize_img[position[1]-SMD_size+i, position[0]-SMD_size+j]
                        self.sum_b = self.sum_b+px[0]
                        self.sum_g = self.sum_g+px[1]
                        self.sum_r = self.sum_r+px[2]
                        self.kk = self.kk + 1
                self.mean_b = self.sum_b/self.kk
                self.mean_g = self.sum_g/self.kk
                self.mean_r = self.sum_r/self.kk

#                print(self.kk, self.mean_r, self.mean_g, self.mean_b, self.mean_h, self.mean_s, self.mean_v)
            
                jfw[path_img][key] = [self.mean_r, self.mean_g, self.mean_b]
                
                b.append(self.mean_b)
                g.append(self.mean_g)
                r.append(self.mean_r)                
                #                color.append([self.mean_r/RGB,self.mean_g/RGB,self.mean_b/RGB])

            if not key == "size":
                if int(key)>=200:
                    b_PCB.append(self.mean_b)
                    g_PCB.append(self.mean_g)
                    r_PCB.append(self.mean_r)

            if not key == "size":
#                if int(key) in self.def_SMD:
#                    df.append([-1, self.mean_r, self.mean_g, self.mean_b])
#                    color.append([0,1,0])
                if int(key)>=200:
                    df.append([key, self.mean_r, self.mean_g, self.mean_b])
                    color.append([1,0,0])
                    
                    # if int(key) in rand:
                    #     df.append([key, self.mean_r, self.mean_g, self.mean_b])
                    #     color.append([1,0,0])
                    # else:
                    #     # if int(key)==204 or  int(key)==205 or  int(key)==206 or  int(key)==208:
                    #     #     print("excluded",key)
                    #     # else:
                    #     not_rand.append([n_rand, key])
                    #     df_ex.append([key, self.mean_r, self.mean_g, self.mean_b])
                    #     color_ex.append([0,0,1])
                    #     n_rand += 1
                elif int(key) in self.def_SMD:
                    df_SMD.append([key, self.mean_r, self.mean_g, self.mean_b])
                    color_SMD.append([0,0,1])
                # else:
                #     df_SMD.append([key, self.mean_r, self.mean_g, self.mean_b])
                #     color_SMD.append([self.mean_r/RGB,self.mean_g/RGB,self.mean_b/RGB])


            if not key == "size":
                if int(key)>=200:
                    tot_g = tot_b+self.mean_b
                    tot_g = tot_g+self.mean_g
                    tot_r = tot_r+self.mean_r
                    N = N+1
                
        mean_b = tot_b/N
        mean_g = tot_g/N
        mean_r = tot_r/N

        # PCA (principal component analysis)
        DF = pd.DataFrame(df).iloc[:,1:]
#        DF_ex = pd.DataFrame(df_ex).iloc[:,1:]
        DF_SMD = pd.DataFrame(df_SMD).iloc[:,1:]        
#        pprint.pprint(DF_SMD)
        #        from pandas import plotting 
#        plotting.scatter_matrix(DF.iloc[:, 1:], figsize=(3, 3), c=list(DF.iloc[:, 0]), alpha=0.5)
#        plt.show()        
#        pprint.pprint(DF_SMD)
        # dfs = DF.iloc[:, 1:].apply(lambda x: (x-x.mean())/x.std(), axis=0)
        # dfs_SMD = DF_SMD.iloc[:, 1:].apply(lambda x: (x-x.mean())/x.std(), axis=0)
#        pprint.pprint(dfs)

        # perform PCA
        pca = PCA()
        pca.fit(DF)
        feature = pca.transform(DF)
#        feature_ex = pca.transform(DF_ex)
        feature_SMD = pca.transform(DF_SMD)
    
        # plot PC (principal component)
        # fig = plt.figure()               
        # self.ax = fig.add_subplot(projection='3d')
#        self.ax.scatter(feature_SMD[:, 0], feature_SMD[:, 1], feature_SMD[:, 2],  s=10, alpha=0.8, c=color_SMD)
#        self.ax.scatter(feature[:, 0], feature[:, 1], feature[:, 2], s=10, alpha=0.8, c=color)
#        self.ax.scatter(feature_ex[:, 0], feature_ex[:, 1], feature_ex[:, 2], s=10, alpha=0.8, c=color_ex)
        # plt.grid()
        # self.ax.set_xlabel("PC1")
        # self.ax.set_ylabel("PC2")
        # self.ax.set_zlabel("PC3")
        # self.ax.set_xlim(-10,10) 
        # self.ax.set_ylim(-10,10) 
        # self.ax.set_zlim(-10,10) 
#        plt.show()

        # contribution rate
        pd.DataFrame(pca.explained_variance_ratio_, index=["PC{}".format(x + 1) for x in range(len(DF.columns))])
        
        mean_b = np.mean(feature, axis=0)[0]
        mean_g = np.mean(feature, axis=0)[1]
        mean_r = np.mean(feature, axis=0)[2]
        
        for ii, value in enumerate(feature):
            diff_b = diff_b+(value[0]-mean_b)**2
            diff_g = diff_g+(value[1]-mean_g)**2
            diff_r = diff_r+(value[2]-mean_r)**2
            # if value[0]>50:
            #     print("large PC2", ii, value)
            # diff_bg = diff_bg+(jfw[path_img][ii][2]-mean_b)*(jfw[path_img][ii][1]-mean_g)
            # diff_gr = diff_gr+(jfw[path_img][ii][0]-mean_r)*(jfw[path_img][ii][1]-mean_g)
            # diff_rb = diff_rb+(jfw[path_img][ii][0]-mean_r)*(jfw[path_img][ii][2]-mean_b)
            
        for jj, value_SMD in enumerate(feature_SMD):
            if value_SMD[0]>50:
                if jj in self.def_SMD:
                    print('def_SMD', jj, value_SMD)
        
        sigma_b = (diff_b/N)**0.5
        sigma_g = (diff_g/N)**0.5
        sigma_r = (diff_r/N)**0.5
        # sigma_bg = diff_bg/N
        # sigma_gr = diff_gr/N
        # sigma_rb = diff_rb/N
        # rho = diff_gr/(N*sigma_g*sigma_r)

        # histogram
        mean_PC = np.mean(feature, axis=0)
        sigma_PC = np.std(feature, axis=0)
        print("mean, std", mean_PC, sigma_PC)

        def func(x, a, mu, sigma):
            return a*np.exp(-(x-mu)**2/(2*sigma**2))

        thre_min = [0] * 3
        thre_max = [0] * 3
        fig = [0] * 3
#        ax = [0] *3
#        fig_hist = plt.figure()
#        ax = fig_hist.add_subplot(111)
        for kk in range(1):
#            ax[kk] = fig_hist.add_subplot(1,3,kk+1)
            hist_h, bins = np.histogram(feature[:,kk])
            bin_width = bins[1] - bins[0]
            bins = bins[:-1]
            param_ini = [60, mean_PC[kk], sigma_PC[kk]]
            popt, pcov = curve_fit(func, bins, hist_h, p0=param_ini)
            print("popt", kk, popt)
            thre_min[kk] = popt[1] - 3 * popt[2]
            thre_max[kk] = popt[1] + 3 * popt[2]
            print("3sigma", thre_min, thre_max)
            fitting = func(bins, popt[0], popt[1], popt[2])
        
        # ax.bar(bins, hist_h, width=bin_width, alpha=0.5, color='m', align='edge')
        # ax.plot(bins, fitting, 'k')
        # ax.set_xlabel('PC1')
        # ax.set_ylabel('# of points')

        #     ax[kk].bar(bins, hist_h, width=bin_width, alpha=0.5, color='m', align='edge')
        #     ax[kk].plot(bins, fitting, 'k')
        #     ax[kk].set_xlabel('PC{}'.format(kk+1))
        # ax[0].set_ylabel('# of points')

        # exclude outlier
        ex_list = []
        ex_PC =[]
        for ii, value in enumerate(feature):
#            flag = [0,0,0]
#            if value[0]>thre_min[0] and value[0]<thre_max[0] and  value[1]>thre_min[1] and value[1]<thre_max[1] and value[2]>thre_min[2] and value[2]<thre_max[2]:
            if value[0]>thre_min[0] and value[0]<thre_max[0]:
#                print("inlier", value)
                ex_PC.append(value)
            else:
                # num_rand = rand[ii]
                # ex_list.append(num_rand)
                # print("outlier", num_rand, value)
                ex_list.append(200+ii)
                print("outlier", 200+ii, value)

        sigma_PC = np.std(ex_PC, axis=0)
        print("sigma_PC", sigma_PC)

        # PCA without outliers

        df_new = []
        df_out = []
        color = []
        color_out = []
        print(ex_list)
        for rgb in df:
            if not int(rgb[0]) in ex_list:
                df_new.append(rgb)
                color.append([1,0,0])
            else:
                df_out.append(rgb)
                color_out.append("red")
        print("df_out",df_out)
                
        DF_new = pd.DataFrame(df_new).iloc[:,1:]
        if df_out:
            DF_out = pd.DataFrame(df_out).iloc[:,1:]
#        DF_ex = pd.DataFrame(df_ex).iloc[:,1:] 
        DF_SMD = pd.DataFrame(df_SMD).iloc[:,1:]

        pca = PCA()
        pca.fit(DF_new)
        feature_new = pca.transform(DF_new)
        if df_out:
            feature_out = pca.transform(DF_out)
#        feature_ex = pca.transform(DF_ex)
        feature_SMD = pca.transform(DF_SMD)

        self.ax.scatter(feature_SMD[:, 0], feature_SMD[:, 1], feature_SMD[:, 2],  s=10, alpha=0.8, c=color_SMD)
        self.ax.scatter(feature_new[:, 0], feature_new[:, 1], feature_new[:, 2], s=10, alpha=0.8, c=color)
#        self.ax.scatter(feature_ex[:, 0], feature_ex[:, 1], feature_ex[:, 2], s=10, alpha=0.8, c=color_ex)
        if df_out:
            self.ax.scatter(feature_out[:, 0], feature_out[:, 1], feature_out[:, 2], s=10, alpha=0.8, c=color_out)
        plt.grid()
        self.ax.set_xlabel("PC1")
        self.ax.set_ylabel("PC2")
        self.ax.set_zlabel("PC3")

        sigma_PC = np.std(feature_new, axis=0)        
        sigma_SMD = np.std(feature_SMD, axis=0)        

        #chi^2 plot

        fig_chi = plt.figure()
        ax_chi = fig_chi.add_subplot(111)
#        ax_chi.set_xlim(0, 100)
        bins = range(0,20,1)

        x_chi = np.linspace(0.5,18.5,19)

        N_new = 0
        fea_new =[]
        fea_SMD = []
        fea_ex = []
        for fea in feature_new:
            chi = fea[0]**2/sigma_PC[0]**2+fea[1]**2/sigma_PC[1]**2+fea[2]**2/sigma_PC[2]**2
            fea_new.append(chi)
            N_new = N_new +1
        # for fea in feature_ex:
        #     chi = fea[0]**2/sigma_PC[0]**2+fea[1]**2/sigma_PC[1]**2+fea[2]**2/sigma_PC[2]**2
        #     fea_ex.append(chi)
        # for fea in feature_SMD:
        #     chi = fea[0]**2/sigma_PC[0]**2+fea[1]**2/sigma_PC[1]**2+fea[2]**2/sigma_PC[2]**2
        #     fea_SMD.append(chi)
        
#        plt.hist([fea_new,fea_SMD],bins=bins, label=["PCB","SMD"])
#        list = [1]

        ret = ax_chi.hist(fea_new,bins=bins)
#       ret = ax_chi.hist(list,bins=bins)
        plt.plot(x_chi, N_new*stats.chi2.pdf(x_chi, 3))
        chi2_pdf = []
        for ll in x_chi:
            chi2_pdf.append(N_new*stats.chi2.pdf(ll, 3))
        print("x_chi2",x_chi)
        print("chi2_pdf",chi2_pdf)
        print("ret",ret[0])

#        result = stats.chisquare(chi2_pdf,ret[0])
#        print("result",result)
        #        plt.legend(loc='upper right')
        ax_chi.set_xlabel("chi^2")
        ax_chi.set_ylabel("# of points")
#        ax_chi.set_xscale('log') 
        
        #list = jfw.flatten()
        json.dump(jfw, fw, indent=2)
        
        #jfw.update(dict)
        
        fr.close()
        fw.close()
        
        #self.ax.set_xlim(0,40)
        #self.ax.set_ylim(0,40)
        #self.ax.set_zlim(0,40)
#        self.ax.scatter(b,g,r,s=20,c=color)
#        self.ax.scatter(g,r,s=20,c=color)
        
        #plt.savefig("rgb.jpg")

        # calculate ellipsoid

        c = 35.4
        alph = c**0.5*sigma_PC[0]
        beta = c**0.5*sigma_PC[1]
        gamma = c**0.5*sigma_PC[2]
        print("abc",alph,beta,gamma)

        theta = np.linspace(0, 2*np.pi, 100)
        phi = np.linspace(0, np.pi, 100)

        ell_x = alph*np.outer(np.cos(theta), np.sin(phi))
        ell_y = beta*np.outer(np.sin(theta), np.sin(phi))
        ell_z = gamma*np.outer(np.ones(np.size(theta)), np.cos(phi))

        self.ax.plot_wireframe(ell_x,ell_y,ell_z,rcount=10, ccount=10, color='orange', alpha=0.5)

        # 楕円体の内部に入っているか判定
        fbg = open("background.txt", "w")
        num_def = 0
        num_nback = 0
        num_nback_ex = 0
        
        for jj, value_SMD in enumerate(feature_SMD):
            PC1 = value_SMD[0]
            PC2 = value_SMD[1]
            PC3 = value_SMD[2]

            conv_PC1 = PC1 / alph
            conv_PC2 = PC2 / beta
            conv_PC3 = PC3 / gamma

            if conv_PC1**2+conv_PC2**2+conv_PC3**2 < 1:
                high_list = [str(jj), "\n"]
                fbg.writelines(high_list)
                num_def += 1

        for jj, value in enumerate(feature_new):
            PC1 = value[0]
            PC2 = value[1]
            PC3 = value[2]

            conv_PC1 = PC1 / alph
            conv_PC2 = PC2 / beta
            conv_PC3 = PC3 / gamma

            if conv_PC1**2+conv_PC2**2+conv_PC3**2 > 1:
                num_nback += 1

        # for jj, value in enumerate(feature_ex):
        #     PC1 = value[0]
        #     PC2 = value[1]
        #     PC3 = value[2]

        #     conv_PC1 = PC1 / alph
        #     conv_PC2 = PC2 / beta
        #     conv_PC3 = PC3 / gamma

        #     if conv_PC1**2+conv_PC2**2+conv_PC3**2 > 1:
        #         num_nback_ex += 1
                
        fbg.close()
        print("number of defects", num_def, num_def*100/164, "%")
        print("number of not background", num_nback, num_nback*100/30, "%")
#        print("number of not background_ex", num_nback_ex, num_nback_ex*100/30, "%")
            
if __name__ == '__main__':
    plot_window = PlotWindow()
    plt.show()
#    app = QApplication(sys.argv)
#    main_window = MainWindow()
#    main_window.show()
#    sys.exit(app.exec_())
