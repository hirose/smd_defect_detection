import os
import shutil
import sys
import datetime
from PyQt5.QtGui import *
from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
from PyQt5.Qt import *
import sip
import numpy as np
import json
import cv2
import pprint

class MainWindow(QWidget):
    def __init__(self, parent=None):
        super(MainWindow, self).__init__(parent)

        self.path_pos = "SMD_position_s.json"
        self.path_bg = "background.txt"
        self.img_path = "module_img/EPECFlex262_vi"
        self.win_size = 4000
        
        self.layout = QGridLayout()
        self.view = QGraphicsView()
        self.scene = QGraphicsScene()
        self.srect = self.view.rect()
        self.view.setScene(self.scene)

        self.view.viewport().setMouseTracking(True)
        self.view.viewport().installEventFilter(self)

        f = open(self.path_pos, "r")
        df = json.load(f)
        f.close()
        h_ref, w_ref, self.L_SMD, self.S_SMD = df["size"]
        img = cv2.imread("./{}_trimmed.jpg".format(self.img_path))
        h, w, d = img.shape[:3]
        self.scale = h_ref/h
        print("scale", self.scale)
        resize_img = cv2.resize(img, dsize=None, fx=self.scale, fy=self.scale)
        h_r, w_r, d_r = resize_img.shape[:3]
        print(f"{img.shape} -> {resize_img.shape}")
        self.img_rgb = cv2.cvtColor(resize_img, cv2.COLOR_BGR2RGB)
        h_rgb, w_rgb, d_rgb = self.img_rgb.shape[:3]
        bytesPerLine = d_r*w_r

#        self.image = QImage(self.img_rgb.data,w_rgb,h_rgb,bytesPerLine,QImage.Format_RGB888)
#        self.pixmap = QtGui.QPixmap.fromImage(self.image)
#        self.scene.addPixmap(self.pixmap)
#        self.item = QGraphicsPixmapItem(QPixmap.fromImage(self.image))
#        self.scene.addItem(self.item)
#        self.view.setScene(self.scene)

#        self.layout.addWidget(self.view, 0, 0)
#        self.setLayout(self.layout)

        self.writePosition(self.path_pos, self.path_bg)
        self.textFromJson(self.path_pos)
#        self.getColor(self.path)

    def writePosition(self, path_pos, path_bg):
        rf = open(path_pos, "r")
        df = json.load(rf)
        rf.close()

        number_bg = []
        fbg = open(path_bg, "r")
        s = fbg.read()
        sl = s.split("\n")
        for line in sl:
            if not line == '':
                number_bg.append(line)
        fbg.close()
        print(number_bg)

        self.img_high = self.img_rgb
        
        for ii in number_bg:
#        for ii in df.keys():
#            if ii != "size":
#                if int(ii)>=200:
            x = df[ii][0]
            y = df[ii][1]
            self.highlight(x,y)
#                    self.writeText(ii, x, y)

#            if ii in number_bg:

    def highlight(self, x, y):
#        self.img_high = cv2.circle(self.img_high, (x,y), 50, (255, 0, 255), -1)
        pass
#        h_high, w_high, d_high = self.img_high.shape[:3]
#        bytesPerLine = d_high*w_high
#        self.image = QImage(self.img_high.data,w_high,h_high,bytesPerLine,QImage.Format_RGB888)
#        self.item = QGraphicsPixmapItem(QPixmap.fromImage(self.image))
#        self.scene.addItem(self.item)
#        self.view.setScene(self.scene)
#        self.layout.addWidget(self.view, 0, 0)
#        self.setLayout(self.layout)        
        

    def getColor(self, path):
        f = open(path, "r")
        jf = json.load(f)

        self.img_circle = self.img_rgb

        thre = 30
        x_ref = int(jf["0"][0])
        y_ref = int(jf["0"][1])
        px_ref = self.img_rgb[y_ref, x_ref]

        r_min = px_ref[0] - thre
        r_max = px_ref[0] + thre
        g_min = px_ref[1] - thre
        g_max = px_ref[1] + thre
        b_min = px_ref[2] - thre
        b_max = px_ref[2] + thre

        x = [0]*4
        y = [0]*4
        px = [0]*4
        for j in range(4):
            x[j] = int(jf["%d"%(j+1)][0])
            y[j] = int(jf["%d"%(j+1)][1])
            px[j] = self.img_rgb[y[j], x[j]]
#        print(px_ref, px1, px2, px3, px4)

            if px[j][0]>r_min and px[j][0]<r_max and px[j][1]>g_min and px[j][1]<g_max and px[j][2]>b_min and px[j][2]<b_max:
                self.img_circle = cv2.circle(self.img_circle, (x[j], y[j]), 3, (0, 255, 0), 3)
                h_cir, w_cir, d_cir = self.img_circle.shape[:3]
                bytesPerLine = d_cir*w_cir
                self.image = QImage(self.img_circle.data,w_cir,h_cir,bytesPerLine,QImage.Format_RGB888)
                self.item = QGraphicsPixmapItem(QPixmap.fromImage(self.image))
                self.scene.addItem(self.item)
                self.view.setScene(self.scene)
                self.layout.addWidget(self.view, 0, 0)
                self.setLayout(self.layout)

        f.close()

    def writeText(self, number, x, y):
        self.img_text = self.img_rgb

        text = str(number)
        self.img_text = cv2.putText(self.img_text, text, (x,y), cv2.FONT_HERSHEY_SIMPLEX, 3.0, (0, 255, 0), 5)
        h_text, w_text, d_text = self.img_text.shape[:3]
        bytesPerLine = d_text*w_text
        self.image = QImage(self.img_text.data,w_text,h_text,bytesPerLine,QImage.Format_RGB888)
        self.item = QGraphicsPixmapItem(QPixmap.fromImage(self.image))
        self.scene.addItem(self.item)
        self.view.setScene(self.scene)
        self.layout.addWidget(self.view, 0, 0)
        self.setLayout(self.layout)

    def textFromJson(self, path):
        f = open(path, "r+")
        jf = json.load(f)

        self.img_jtext = self.img_high

        for key in jf:
            position = jf[key]
            x = position[0]
            y = position[1]
#            self.img_jtext = cv2.putText(self.img_jtext, key, (x,y), cv2.FONT_HERSHEY_SIMPLEX, 3.0, (0, 255, 0), 5)
            if position[2] == "s":
                SMD_size = self.S_SMD
            else:
                SMD_size = self.L_SMD
            self.img_jtext = cv2.rectangle(self.img_jtext, (x-SMD_size,y-SMD_size), (x+SMD_size,y+SMD_size), (255,255,0),3)

        imgj_resize = cv2.resize(self.img_jtext,(self.win_size,self.win_size))
        h_jtext, w_jtext, d_jtext = imgj_resize.shape[:3]
        bytesPerLine = d_jtext*w_jtext
        self.image = QImage(imgj_resize.data,w_jtext,h_jtext,bytesPerLine,QImage.Format_RGB888)
        self.item = QGraphicsPixmapItem(QPixmap.fromImage(self.image))
        self.scene.addItem(self.item)
        self.view.setScene(self.scene)
        self.layout.addWidget(self.view, 0, 0)
        self.setLayout(self.layout)

#        self.writeColor(path)
        f.close()

        resize_img = cv2.resize(self.img_jtext, dsize=None, fx=1/self.scale, fy=1/self.scale)
        img_out = cv2.cvtColor(resize_img, cv2.COLOR_BGR2RGB)
        cv2.imwrite(self.img_path+"_rectOnly.png", img_out)

    def writeColor(self, path):
        f = open(path, "r+")
        jf = json.load(f)
        
        for key in jf:
            print(key)
            s = input()
            jf[key]["color"] = s

        json.dump(jf, f, indent=2)
        f.close()
            
if __name__ == '__main__':
    app = QApplication(sys.argv)
    main_window = MainWindow()
    main_window.show()
    sys.exit(app.exec_())
