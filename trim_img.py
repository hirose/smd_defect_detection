from PyQt5.QtGui import *
from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
from PyQt5.Qt import *
import sys
import sip
import numpy as np
import cv2
import pprint
import math
import json

class MainWindow(QWidget):
    def __init__(self, parent=None):
        super(MainWindow, self).__init__(parent)

        self.path = "./module_img/EPECFlex262_vi"
        self.conf_path = "./ref.json"
        
        self.pos1_x = -1
        self.pos1_y = -1
        self.pos2_x = -1
        self.pos2_y = -1        
        self.pos3_x = -1
        self.pos3_y = -1
        self.ii = 0
        self.n_click = 5
        self.mean_x = 0
        self.mean_y = 0
        
        self.layout = QGridLayout()
        self.view = QGraphicsView()
        self.scene = QGraphicsScene()
        self.srect = self.view.rect()
        self.view.setScene(self.scene)

        self.view.viewport().setMouseTracking(True)
        self.view.viewport().installEventFilter(self)

        self.scale = 1
        self.img = cv2.imread(self.path+".jpeg")
        self.h, self.w, self.d = self.img.shape[:3]
        resize_img = cv2.resize(self.img, dsize=None, fx=self.scale, fy=self.scale)
        h_r, w_r, d_r = resize_img.shape[:3]
        self.img_rgb = cv2.cvtColor(resize_img, cv2.COLOR_BGR2RGB)
        h_rgb, w_rgb, d_rgb = self.img_rgb.shape[:3]
        bytesPerLine = d_r*w_r

        self.image = QImage(self.img_rgb.data,w_rgb,h_rgb,bytesPerLine,QImage.Format_RGB888)
        self.item = QGraphicsPixmapItem(QPixmap.fromImage(self.image))
        self.scene.addItem(self.item)
        self.view.setScene(self.scene)

        self.layout.addWidget(self.view, 0, 0)
        self.setLayout(self.layout)

    def eventFilter(self, object, event):
        if event.type() == QEvent.MouseButtonPress and object is self.view.viewport():
            if event.button() == Qt.LeftButton:
                pos = self.view.mapToScene(event.pos())
                print('MouseButtonPress: (%d, %d)' % (pos.x(), pos.y()))
                # if self.ii<self.n_click-1:
                #     print(self.ii)
                #     self.mean_x = self.mean_x+pos.x() 
                #     self.mean_y = self.mean_y+pos.y()
                #     self.ii = self.ii+1
                # elif self.ii==self.n_click-1:
                #     if self.pos1_x == -1:
                #         print("pos1", self.ii)
                #         self.mean_x = self.mean_x+pos.x() 
                #         self.mean_y = self.mean_y+pos.y()
                #         self.pos1_x = self.mean_x/self.n_click
                #         self.pos1_y = self.mean_y/self.n_click
                #         print(self.pos1_x, self.pos1_y)
                #         self.mean_x = 0
                #         self.mean_y = 0
                #         self.ii = 0
                #     elif self.pos2_x == -1:
                #         print("pos2", self.ii)
                #         self.mean_x = self.mean_x+pos.x() 
                #         self.mean_y = self.mean_y+pos.y()
                #         self.pos2_x = self.mean_x/self.n_click
                #         self.pos2_y = self.mean_y/self.n_click
                #         print(self.pos2_x, self.pos2_y)
                #         self.mean_x = 0
                #         self.mean_y = 0
                #         self.ii = 0
                #     elif self.pos3_x == -1:
                #         print("pos3", self.ii)
                #         self.mean_x = self.mean_x+pos.x() 
                #         self.mean_y = self.mean_y+pos.y()
                #         self.pos3_x = self.mean_x/self.n_click
                #         self.pos3_y = self.mean_y/self.n_click
                #         print(self.pos3_x, self.pos3_y)
                #         self.trimImg(self.pos1_x,self.pos1_y,self.pos2_x,self.pos2_y,self.pos3_x,self.pos3_y)
                
                if self.pos1_x == -1:
                    self.pos1_x = pos.x()
                    self.pos1_y = pos.y()
                elif self.pos2_x == -1:
                    self.pos2_x = pos.x()
                    self.pos2_y = pos.y()
                elif self.pos3_x == -1:
                    self.pos3_x = pos.x()
                    self.pos3_y = pos.y()
                    self.trimImg(self.conf_path, self.pos1_x,self.pos1_y,self.pos2_x,self.pos2_y,self.pos3_x,self.pos3_y)
        return QWidget.eventFilter(self, object, event)

    def trimImg(self, path, x1, y1, x2, y2, x3, y3):

        ref_f = open(path, "r")
        ref = json.load(ref_f)
        ref_f.close()
        
        x1 = x1/self.scale
        x2 = x2/self.scale
        x3 = x3/self.scale

        y1 = y1/self.scale
        y2 = y2/self.scale
        y3 = y3/self.scale

        p1x = ref["p1"][0]
        p1y = ref["p1"][1]
        p2x = ref["p2"][0]
        p2y = ref["p2"][1]
        p3x = ref["p3"][0]
        p3y = ref["p3"][1]
        ref_h = ref["height"]
        ref_w = ref["width"]

        l_ref = ((p3x-p1x)**2+(p3y-p1y)**2)**0.5
        l = ((x3-x1)**2+(y3-y1)**2)**0.5
        l_scale = l_ref/l
        # vec_ref = np.array([[(p3x-p1x)/l_scale, (p3y-p1y)/l_scale,]]).T
        # vec = np.array([[x3-x1, y3-y1]]).T
        # vec_inv = np.linalg.pinv(vec)
        # print("E",np.dot(vec_inv, vec))
        # rot = np.dot(vec_ref, vec_inv)
        # rot_mat = np.append(rot,np.array([[0,0]]).T,axis=1)
        # #        rot_mat = rot[:2,:3]
        # print("rot",rot)
        # print("rot_mat",rot_mat)
        
        rot_center = (int((x1+x3)/2), int((y1+y3/2)))
        angle1 = math.degrees(math.atan((x3-x1)/(y3-y1)))
        angle1_ref = math.degrees(math.atan((p3x-p1x)/(p3y-p1y)))
        angle2 = math.degrees(math.atan((x3-x2)/(y3-y2)))
        angle2_ref = math.degrees(math.atan((p3x-p2x)/(p3y-p2y)))
        rot_angle1 = angle1_ref - angle1
        print("rot_angle1", rot_angle1)
        rot_angle2 = angle2_ref - angle2
        print("rot_angle2", rot_angle2)
        rot_angle = (rot_angle1+rot_angle2)/2
        print("rot_angle", rot_angle)        
        mat = cv2.getRotationMatrix2D(rot_center, rot_angle, 1)
        img_rotate = cv2.warpAffine(self.img, mat, (self.w, self.h))
        cv2.imwrite("rotate_img.jpg", img_rotate)

        # vec1 = np.array([[x1,y1]]).T
        # vec2 = np.array([[x2,y2]]).T
        # vec3 = np.array([[x3,y3]]).T
        # x1, y1 = np.dot(rot, vec1)
        # x2, y2 = np.dot(rot, vec2)
        # x3, y3 = np.dot(rot, vec3)
        x1, y1 = self.rotCoordinate(x1, y1, rot_center, -rot_angle)
        x2, y2 = self.rotCoordinate(x2, y2, rot_center, -rot_angle)
        x3, y3 = self.rotCoordinate(x3, y3, rot_center, -rot_angle)

        centerx = (x1+x2)/2.
        centery = ((y1+y2)/2+y3)/2.

        self.img_cir = cv2.circle(self.img, (int(centerx), int(centery)), 50, (255, 0, 255), -1)
        self.img_cir = cv2.circle(self.img, (int(x1), int(y1)), 10, (255, 0, 255), -1)
        self.img_cir = cv2.circle(self.img, (int(x2), int(y2)), 10, (255, 0, 255), -1)
        self.img_cir = cv2.circle(self.img, (int(x3), int(y3)), 10, (255, 0, 255), -1)
        h_c, w_c, d_c = self.img_cir.shape[:3]
        self.img_rgb = cv2.cvtColor(self.img_cir, cv2.COLOR_BGR2RGB)
        h_rgb, w_rgb, d_rgb = self.img_rgb.shape[:3]
        bytesPerLine = d_c*w_c

        self.image = QImage(self.img_rgb.data,w_rgb,h_rgb,bytesPerLine,QImage.Format_RGB888)
        self.item = QGraphicsPixmapItem(QPixmap.fromImage(self.image))
        self.scene.addItem(self.item)
        self.view.setScene(self.scene)
        
        width = ref_h/(2*l_scale)

        xmin = int(centerx-width)
        xmax = int(centerx+width)
        ymin = int(centery-width)
        ymax = int(centery+width)

        img_trimmed = img_rotate[ymin : ymax, xmin : xmax]
        cv2.imwrite(self.path+"_trimmed.jpg", img_trimmed)
#        cv2.imwrite(self.path+"_rotTest.jpg", img_trimmed)

    def rotCoordinate(self, x, y, center, angle):
        p0 = np.array(center)
        p1 = np.array([x,y])
        v = p1-p0
        rot_m = np.array([[np.cos(math.radians(angle)), -np.sin(math.radians(angle))],[np.sin(math.radians(angle)), np.cos(math.radians(angle))]])
        u = np.dot(rot_m, v)
        uc = u+p0
        return uc[0], uc[1]
        
if __name__ == '__main__':
    app = QApplication(sys.argv)
    main_window = MainWindow()
    main_window.show()
    sys.exit(app.exec_())
