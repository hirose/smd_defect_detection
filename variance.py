import os
import shutil
import sys
import datetime
from PyQt5.QtGui import *
from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
from PyQt5.Qt import *
import sip
import numpy as np
import cv2
import pprint
import matplotlib.pyplot as plt
from matplotlib import patches
import numpy as np
from mpl_toolkits.mplot3d import Axes3D
import json
import cv2
import math

class PlotWindow:
    def __init__(self):
        fig = plt.figure()
        self.ax = fig.add_subplot(projection='3d')
        # self.ax = fig.add_subplot(111)
        # self.ax.set_aspect('equal')
        self.ax.set_xlabel("B")
        self.ax.set_ylabel("G")
        self.ax.set_zlabel("R")
        # self.ax.set_xlabel("R")
        # self.ax.set_ylabel("G")
#        self.ax.set_xlim(15, 40)
#        self.ax.set_ylim(0, 20)
        # self.ax.set_xlim(0, 160)
        # self.ax.set_ylim(0, 160)
#        self.ax.set_zlim(0, 160)

        self.sum_b = 0
        self.sum_g = 0
        self.sum_r = 0
        self.kk = 0

        self.path_img = "KEKv1.1Q5WBAP_vi"
        self.path_pos = "SMD_position_s.json"
        self.path_color = "SMD_color.json"
        self.read_img(self.path_img, self.path_pos)
        self.json(self.path_img, self.path_pos, self.path_color)
        devb = []
        devg = []
        devr = []
        
    def read_img(self, path_img, path_pos):
        f = open(path_pos, "r")
        df = json.load(f)
        f.close()
        h_ref, w_ref, self.L_SMD, self.S_SMD = df["size"]
        img = cv2.imread("./module_img/{}_trimmed.jpg".format(path_img))
        h,w,d = img.shape[:3]
        scale = h_ref/h
        print("scale",scale)
        self.resize_img = cv2.resize(img, dsize=None, fx=scale, fy=scale)

    def json(self, path_img, path_pos, path_color):

        fr = open(path_pos, "r")
        jfr = json.load(fr)
        fw = open(path_color, "w")
        jfw = {path_img:{"0":[0,0,0]}}
        ftext = open(path_img+".txt", "w")
        b = []
        g = []
        r = []
        devb = []
        devg = []
        devr = []
        color = []
        RGB = 256
        tot_g = 0
        tot_r = 0
        diff_g = 0
        diff_r = 0
        diff_gr = 0
        N = 0
        px = {}
        self.dev_list = []

        f = open('all_var.txt','a')
        
        
        for key in jfr:
            if not key == "size":
                if int(key) < 200:
                    self.sum_b = 0            
                    self.sum_g = 0            
                    self.sum_r = 0
                    self.var_b = 0
                    self.var_g = 0
                    self.var_r = 0
                    self.kk = 0            
                    position = jfr[key]
                    B_val = []
                    G_val = []
                    R_val = []
                    val_color = []
                    if jfr[key][2] == "s":
                        SMD_size = self.S_SMD
                    else:
                        SMD_size = self.L_SMD
                    ran = 1+2*SMD_size
                    #    print(position)
                    for i in range(ran):
                        for j in range(ran):
                            px[self.kk] = self.resize_img[position[1]-SMD_size+i, position[0]-SMD_size+j]
                            self.sum_b = self.sum_b+px[self.kk][0]
                            self.sum_g = self.sum_g+px[self.kk][1]
                            self.sum_r = self.sum_r+px[self.kk][2]
                            B_val.append(px[self.kk][0])
                            G_val.append(px[self.kk][1])
                            R_val.append(px[self.kk][2])
                            val_color.append([px[self.kk][2]/RGB,px[self.kk][1]/RGB,px[self.kk][0]/RGB])
                            self.kk = self.kk + 1
                            #                print("kk is", self.kk)
                    self.mean_b = self.sum_b/self.kk
                    self.mean_g = self.sum_g/self.kk
                    self.mean_r = self.sum_r/self.kk
                    for k in range(self.kk):
                        self.var_b = self.var_b+(px[k][0]-self.mean_b)**2
                        self.var_g = self.var_g+(px[k][1]-self.mean_g)**2
                        self.var_r = self.var_r+(px[k][2]-self.mean_r)**2
                    self.dev_b = (self.var_b/self.kk)**0.5
                    self.dev_g = (self.var_g/self.kk)**0.5
                    self.dev_r = (self.var_r/self.kk)**0.5
                    devb.append(self.dev_b)
                    devg.append(self.dev_g)
                    devr.append(self.dev_r)
                    self.dev = (self.dev_b**2+self.dev_g**2+self.dev_r**2)**0.5
                    self.dev_list.append(self.dev)
#                    f.writelines([str(self.dev),'\n'])
                    if self.dev > 50:
                        print("large dev", key, self.dev)
#                    ftext.write(key, self.dev)
                    if int(key)==28 or int(key)==64 or int(key)==96 or int(key)==134:
                        print("no SMD", key, self.dev)
                    if int(key)==62:
                        self.ax.scatter(R_val,G_val,B_val,s=5,c=val_color)
                        print("scat",key,self.dev_r, self.dev_g)
#                    ftext.write(key, self.dev)
                
#                print(self.kk, self.mean_r, self.mean_g, self.mean_b, self.var)
            
                    jfw[path_img][key] = [self.mean_r, self.mean_g, self.mean_b]
            
                    b.append(self.mean_b)
                    g.append(self.mean_g)
                    r.append(self.mean_r)
#                    color.append([self.mean_r/RGB,self.mean_g/RGB,self.mean_b/RGB])

            
            if not key == "size":
                if int(key) < 200:
                    if int(key)==28 or int(key)==64 or int(key)==134 or int(key)==96:
                        color.append([1,0,0])
                    else:
                        #                    color.append([0,1,0])
                        color.append([self.mean_r/RGB,self.mean_g/RGB,self.mean_b/RGB])


#             if not key == "size":
#                 if int(key)>=200:
#                     tot_g = tot_g+self.mean_g
#                     tot_r = tot_r+self.mean_r
#                     N = N+1

        f.close()


#         mean_g = tot_g/N
#         mean_r = tot_r/N
        
#         for ii in jfw[path_img]:
#             if not ii == "size":
#                 if int(ii)>=200:
#                     diff_g = diff_g+(jfw[path_img][ii][1]-mean_g)**2
#                     diff_r = diff_r+(jfw[path_img][ii][0]-mean_r)**2
#                     diff_gr = diff_gr+(jfw[path_img][ii][0]-mean_r)*(jfw[path_img][ii][1]-mean_g)

#         sigma_g = (diff_g/N)**0.5
#         sigma_r = (diff_r/N)**0.5
#         rho = diff_gr/(N*sigma_g*sigma_r)

# #        print(sigma_g, sigma_r, rho)
        
#         #list = jfw.flatten()
#         json.dump(jfw, fw, indent=2)
        
        #jfw.update(dict)
        
        fr.close()
        fw.close()

        fig_hist = plt.figure()
        ax_hist = fig_hist.add_subplot(111)
        ax_hist.set_xlabel("sigma")
        ax_hist.set_ylabel("# of ponts")
        plt.hist(self.dev_list, bins=20)
        
#        plt.hist(self.dev_list, bins=50, range=(0, 100), log=True)
        
        #self.ax.set_xlim(0,40)
        #self.ax.set_ylim(0,40)
        #self.ax.set_zlim(0,40)
#        self.ax.scatter(devb,devg,devr,s=20,c=color)
#        self.ax.scatter(devr,devg,s=2,c=color)
        
        #plt.savefig("rgb.jpg")
        
#    def gaus2d(x,y,mean_x,mean_y,sigma_x,sigma_y,cov):
#        z = np.exp((((x-mean_x)/sigma_x)**2+(((y-mean_y)/sigma_y)**2-2*cov*(x-mean_x)*(y-mean_y)/(sigma_x*sigma_y)))/(2*(1-cov**2)))/(2*math.pi*sigma_x*sigma_y*(1-cov**2)**0.5)
#        return z

#    X, Y = np.mgrid[20:40:100j, 0:20:100j]
#    Z = gaus2d(X,Y,mean_g,mean_r,sigma_g,sigma_r,rho)
    
    #fig1 = plt.figure()
    #ax1 = fig1.add_subplot(121)
    #ax.contour(X,Y,Z,levels=[0.68,0.95,0.99,1-5.7*10**(-7),1])
    
    # calculate ellipse
#         c = 3.035*2
#         sigma_u = ((sigma_g**2+sigma_r**2+((sigma_g**2-sigma_r**2)**2+4*sigma_g*sigma_r)**0.5)/2)**0.5
#         sigma_n = ((sigma_g**2+sigma_r**2-((sigma_g**2-sigma_r**2)**2+4*sigma_g*sigma_r)**0.5)/2)**0.5
#         ax_long = 2*c*sigma_u**0.5
#         ax_short = 2*c*sigma_n**0.5
#         alpha = math.degrees(math.atan((sigma_g**2-sigma_r**2)/rho))
# #        print(alpha)
#         patch = patches.Ellipse(xy=(mean_g, mean_r), width=ax_long, height=ax_short, angle=alpha, ec="red", fill=False)
# #        self.ax.add_patch(patch)

#         fc = open(path_color, "r")
#         jfc = json.load(fc)
#         jfcolor = jfc[path_img]

#         fbg = open("background.txt", "w")
#         boad = 0
#         fail = 0
        
#         for jj in jfcolor:
#             G = jfcolor[jj][1]
#             R = jfcolor[jj][0]
            
#             pg = (G-mean_g)*math.cos(math.radians(alpha))+(R-mean_r)*math.sin(math.radians(alpha))
#             pr = (ax_long/ax_short)*(-(G-mean_g)*math.sin(math.radians(alpha))+(R-mean_r)*math.cos(math.radians(alpha)))
            
#             if pg**2+pr**2<=(ax_long/2)**2:
# #                print("background is",jj)
#                 if int(jj) >= 200:
#                     boad = boad+1
#                 else:
#                     fail = fail+1
#                 list = [jj, "\n"]
#                 fbg.writelines(list)

#         print("boad", boad*100/25, "%")
#         print("fail", fail*100/164, "%", fail)
#         fc.close()
#         fbg.close()
        

if __name__ == '__main__':
    plot_window = PlotWindow()
    plt.show()
#    app = QApplication(sys.argv)
#    main_window = MainWindow()
#    main_window.show()
#    sys.exit(app.exec_())
